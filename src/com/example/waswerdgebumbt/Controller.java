package com.example.waswerdgebumbt;


import android.app.Application;
 
public class Controller extends Application{
     
    private String name="CONTROLLER";
    private String email="EMAIL";
     
 
    public String getName() {
         
        return name;
    }
     
    public void setName(String aName) {
        
       name = aName;
         
    }
    
    public String getEmail() {
         
        return email;
    }
     
    public void setEmail(String aEmail) {
        
      email = aEmail;
    }
 
}